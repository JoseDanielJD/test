import React from "react";
import "./App.css";
import { Editor } from "slate-react";
import { Value } from "slate";

const initialValue = Value.fromJSON({
  document: {
    nodes: [
      {
        object: "block",
        type: "paragraph",
        nodes: [
          {
            object: "text",
            text: "This is editable "
          },
          {
            object: "text",
            text: "rich",
            marks: [{ type: "bold" }]
          },
          {
            object: "text",
            text: " text, "
          },
          {
            object: "text",
            text: "much",
            marks: [{ type: "italic" }]
          },
          {
            object: "text",
            text: " better than a "
          },
          {
            object: "text",
            text: "<textarea>",
            marks: [{ type: "code" }]
          },
          {
            object: "text",
            text: "!"
          }
        ]
      },
      {
        object: "block",
        type: "paragraph",
        nodes: [
          {
            object: "text",
            text:
              "Since it's rich text, you can do things like turn a selection of text "
          },
          {
            object: "text",
            text: "bold",
            marks: [{ type: "bold" }]
          },
          {
            object: "text",
            text:
              ", or add a semantically rendered block quote in the middle of the page, like this:"
          }
        ]
      },
      {
        object: "block",
        type: "block-quote",
        nodes: [
          {
            object: "text",
            text: "A wise quote."
          }
        ]
      },
      {
        object: "block",
        type: "paragraph",
        nodes: [
          {
            object: "text",
            text: "Try it out for yourself!"
          }
        ]
      }
    ]
  }
});

class App extends React.Component {
  state = {
    value: initialValue
  };

  onChange = ({ value }) => {
    this.setState({ value });
  };

  onKeyDown = (event, editor, next) => {
    if (event.key !== "&") return next();
    event.preventDefault();
    editor.insertText(" ...and... ");
  };

  render() {
    const toggleBold = event => {
      event.preventDefault();
      this.refs.editor1.toggleMark("bold");
    };

    const toggleItalic = event => {
      event.preventDefault();
      this.refs.editor1.toggleMark("italic");
    };

    const toggleUnderline = event => {
      event.preventDefault();
      this.refs.editor1.toggleMark("underline");
    };
    const toggleCode = event => {
      event.preventDefault();
      this.refs.editor1.toggleMark("code");
    };

    const renderMark = (props, editor, next) => {
      const { children, mark, attributes } = props;
      switch (mark.type) {
        case "bold":
          return <strong {...attributes}>{children}</strong>;
        case "italic":
          return <em {...attributes}>{children}</em>;
        case "underline":
          return <u {...attributes}>{children}</u>;
        default:
          return next();
      }
    };

    return (
      <div className="App">
        <h1>Test for slate</h1>
        <div className="buttons">
          <h4>hotkeys:</h4> <h5>'&' for add 'and' were exist the cursor</h5>
          <h4>buttons:</h4>
          <button onMouseDown={toggleBold}>Bold</button>
          <button onMouseDown={toggleItalic}>Italic</button>
          <button onMouseDown={toggleUnderline}>Underline</button>
        </div>
        <div className="editor1">
          <Editor
            ref={"editor1"}
            value={this.state.value}
            onChange={this.onChange}
            onKeyDown={this.onKeyDown}
            renderMark={renderMark}
          />
        </div>
      </div>
    );
  }
}

export default App;
